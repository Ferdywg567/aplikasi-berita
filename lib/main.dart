// ignore_for_file: prefer_const_constructors, unused_local_variable, avoid_print, unused_import, unrelated_type_equality_checks

import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _current = 0;
  final CarouselController _controller = CarouselController();
  Future getHeadlines() async {
    var keyApi = "fdd0c78b51684898aa426d9477eb459e";
    var dataJson = await http.get(Uri.parse(
        "https://newsapi.org/v2/top-headlines?country=id&apiKey=$keyApi"));

    var data = jsonDecode(dataJson.body)['articles'] as List;
    // return data.map((i) => Text(i['title'])).toList();
    return data
        .map((i) => Container(
              child: Container(
                margin: EdgeInsets.all(5.0),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          color: Colors.blue,
                          width: double.infinity,
                        ),
                        (i['urlToImage'] != null
                            ? Image.network(i['urlToImage'].toString(),
                                fit: BoxFit.cover, width: 1000.0)
                            : FlutterLogo()),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: const [
                                  Color.fromARGB(200, 0, 0, 0),
                                  Color.fromARGB(0, 0, 0, 0)
                                ],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                              ),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0),
                            child: Text(
                              'No. ${i['title']} image',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Berita1'),
      ),
      body: FutureBuilder(
        //method to be waiting for in the future
        future: getHeadlines(),
        builder: (contx, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var list = snapshot.data;
            return ListView(
              children: [
                Expanded(
                    child: CarouselSlider(
                  items: list,
                  carouselController: _controller,
                  options: CarouselOptions(
                    autoPlay: true,
                    enlargeCenterPage: true,
                    aspectRatio: 16 / 9,
                  ),
                )),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          topLeft: Radius.circular(30.0)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // Shadow position
                        ),
                      ]),
                  width: double.infinity,
                  child: Column(
                    children: [
                      Text.rich(
                        TextSpan(
                          text: '# Trending',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ), // default text style
                          children: const <TextSpan>[
                            TextSpan(text: 'Tags', style: TextStyle()),
                          ],
                        ),
                        textAlign: TextAlign.left,
                      ),
                      GridView.count(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        childAspectRatio: 6 / 1,
                        crossAxisCount: 3,
                        padding: EdgeInsets.all(10),
                        children: List.generate(12, (index) {
                          return Center(
                            child: Text(
                              'MakananEnak',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                ),
                Container(
                  //apply margin and padding using Container Widget.
                  padding:
                      EdgeInsets.all(20), //You can use EdgeInsets like above
                  margin: EdgeInsets.all(5),
                  child: Text(
                    "Sports",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                    child: CarouselSlider(
                  items: list,
                  carouselController: _controller,
                  options: CarouselOptions(
                    // autoPlay: true,
                    // enlargeCenterPage: true, 
                    aspectRatio: 16 / 9,
                  ),
                )),
                Container(
                  //apply margin and padding using Container Widget.
                  padding:
                      EdgeInsets.all(20), //You can use EdgeInsets like above
                  margin: EdgeInsets.all(5),
                  child: Text(
                    "Sports",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                    child: CarouselSlider(
                  items: list,
                  carouselController: _controller,
                  options: CarouselOptions(
                    // autoPlay: true,
                    // enlargeCenterPage: true,
                    aspectRatio: 16 / 9,
                  ),
                )),
                Container(
                  //apply margin and padding using Container Widget.
                  padding:
                      EdgeInsets.all(20), //You can use EdgeInsets like above
                  margin: EdgeInsets.all(5),
                  child: Text(
                    "Sports",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                    child: CarouselSlider(
                  items: list,
                  carouselController: _controller,
                  options: CarouselOptions(
                    // autoPlay: true,
                    // enlargeCenterPage: true,
                    aspectRatio: 16 / 9,
                  ),
                )),
                Container(
                  //apply margin and padding using Container Widget.
                  padding:
                      EdgeInsets.all(20), //You can use EdgeInsets like above
                  margin: EdgeInsets.all(5),
                  child: Text(
                    "Sports",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                    child: CarouselSlider(
                  items: list,
                  carouselController: _controller,
                  options: CarouselOptions(
                    // autoPlay: true,
                    // enlargeCenterPage: true,
                    aspectRatio: 16 / 9,
                  ),
                )),
              ],
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    ));
  }
}
